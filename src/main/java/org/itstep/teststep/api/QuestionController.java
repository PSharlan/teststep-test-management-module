package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.itstep.teststep.model.Question;
import org.itstep.teststep.service.QuestionService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/questions")
@Api(value = "/api/v1/questions", description = "Manage questions")
public class QuestionController {

    private QuestionService questionService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Return all existing questions")
    public List<Question> getAllQuestions() {
        return questionService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/ready", method = RequestMethod.GET)
    @ApiOperation(value = "Return all questions ready to use")
    public List<Question> getReadyQuestions() {
        return questionService.findReady();
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return question by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Question getQuestionById(
            @ApiParam(value = "Id of a question to lookup for", required = true)
            @PathVariable String id) {
        return questionService.findById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return list of questions by params")
    @RequestMapping(value = "/params", method = RequestMethod.GET)
    public List<Question> getQuestionsByParams(@RequestParam String... params) {
        return questionService.findByParams(params);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return limited list of questions by params")
    @RequestMapping(value = "amount/{number}/params", method = RequestMethod.GET)
    public List<Question> getQuestionsByParams(
            @ApiParam(value = "Amount of a questions to lookup for", required = true)
            @PathVariable int number,
            @RequestParam String... params) {
        return questionService.findByParams(number, params);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return list of ready questions by params")
    @RequestMapping(value = "ready/params", method = RequestMethod.GET)
    public List<Question> getReadyQuestionsByParams(@RequestParam String... params) {
        return questionService.findReadyByParams(params);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return limited list of ready questions by params")
    @RequestMapping(value = "ready/amount/{number}/params", method = RequestMethod.GET)
    public List<Question> getReadyQuestionsByParams(
            @ApiParam(value = "Amount of a questions to lookup for", required = true)
            @PathVariable int number,
            @RequestParam String... params) {
        return questionService.findReadyByParams(number, params);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "authors/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Return all questions by author id")
    public List<Question> getQuestionByAuthorId(
            @ApiParam(value = "Id of an author to lookup for", required = true)
            @PathVariable long id) {
        return questionService.findByAuthorId(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create question", notes = "Required question instance")
    public Question createQuestion(
            @ApiParam(value = "Question instance", required = true)
            @Valid @RequestBody Question question) {
        return questionService.save(question);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete question by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteQuestionById(
            @ApiParam(value = "Id of a question to delete", required = true)
            @PathVariable String id) {
        questionService.delete(id);
    }

}
