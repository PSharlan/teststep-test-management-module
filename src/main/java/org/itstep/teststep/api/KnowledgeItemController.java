package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.itstep.teststep.model.KnowledgeItem;
import org.itstep.teststep.service.KnowledgeItemService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/items")
@Api(value = "/api/v1/items", description = "Manage knowledge items")
public class KnowledgeItemController {

    private KnowledgeItemService knowledgeItemService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Return all existing items")
    public List<KnowledgeItem> getAllItems() {
        return knowledgeItemService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return item by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public KnowledgeItem getItemById(
            @ApiParam(value = "Id of an item to lookup for", required = true)
            @PathVariable String id) {
        return knowledgeItemService.findById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return list of itmes by language")
    @RequestMapping(value = "/language", method = RequestMethod.GET)
    public List<KnowledgeItem> getItemsByLanguage(@RequestParam String language) {
        return knowledgeItemService.findByLanguageType(language);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create item", notes = "Required item instance")
    public KnowledgeItem createItem(
            @ApiParam(value = "Item instance", required = true)
            @Valid @RequestBody KnowledgeItem item) {
        return knowledgeItemService.save(item);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Delete item by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItemById(
            @ApiParam(value = "Id of an item to delete", required = true)
            @PathVariable String id) {
        knowledgeItemService.delete(id);
    }
}
