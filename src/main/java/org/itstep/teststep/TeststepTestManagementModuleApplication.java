package org.itstep.teststep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeststepTestManagementModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeststepTestManagementModuleApplication.class, args);
	}
}
