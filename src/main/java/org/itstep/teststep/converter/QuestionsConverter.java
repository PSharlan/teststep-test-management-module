package org.itstep.teststep.converter;

import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.QuestionEntity;
import org.itstep.teststep.model.Question;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class QuestionsConverter {

    final ModelMapper modelMapper;

    public QuestionEntity toEntity(Question question) {
        return modelMapper.map(question, QuestionEntity.class);
    }

    public Question toModel(QuestionEntity question) {
        return modelMapper.map(question, Question.class);
    }

    public List<Question> toModels(List<QuestionEntity> questions) {
        return questions.stream().map(this::toModel).collect(Collectors.toList());
    }

    public List<QuestionEntity> toEntities(List<Question> questions) {
        return questions.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
