package org.itstep.teststep.converter;

import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.KnowledgeItemEntity;
import org.itstep.teststep.model.KnowledgeItem;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class KnowledgeItemConverter {

    final ModelMapper modelMapper;

    public KnowledgeItemEntity toEntity(KnowledgeItem KnowledgeItem) {
        return modelMapper.map(KnowledgeItem, KnowledgeItemEntity.class);
    }

    public KnowledgeItem toModel(KnowledgeItemEntity KnowledgeItem) {
        return modelMapper.map(KnowledgeItem, KnowledgeItem.class);
    }

    public List<KnowledgeItem> toModels(List<KnowledgeItemEntity> KnowledgeItems) {
        return KnowledgeItems.stream().map(this::toModel).collect(Collectors.toList());
    }

    public List<KnowledgeItemEntity> toEntities(List<KnowledgeItem> KnowledgeItems) {
        return KnowledgeItems.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
