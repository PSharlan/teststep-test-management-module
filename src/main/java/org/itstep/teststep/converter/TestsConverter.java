package org.itstep.teststep.converter;

import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.TestEntity;
import org.itstep.teststep.model.Test;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class TestsConverter {

    final ModelMapper modelMapper;

    public TestEntity toEntity(Test test) {
        return modelMapper.map(test, TestEntity.class);
    }

    public Test toModel(TestEntity test) {
        return modelMapper.map(test, Test.class);
    }

    public List<Test> toModels(List<TestEntity> tests) {
        return tests.stream().map(this::toModel).collect(Collectors.toList());
    }

    public List<TestEntity> toEntities(List<Test> tests) {
        return tests.stream().map(this::toEntity).collect(Collectors.toList());
    }
}
