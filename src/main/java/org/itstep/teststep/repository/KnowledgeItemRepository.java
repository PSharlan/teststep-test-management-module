package org.itstep.teststep.repository;

import org.itstep.teststep.entity.KnowledgeItemEntity;
import org.itstep.teststep.entity.LanguageTypeEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface KnowledgeItemRepository  extends MongoRepository<KnowledgeItemEntity, String> {

    List<KnowledgeItemEntity> findAllByLanguageType(LanguageTypeEntity languageType);
}
