package org.itstep.teststep.repository;

import org.itstep.teststep.entity.*;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface QuestionRepository extends MongoRepository<QuestionEntity, String> {

    List<QuestionEntity> findAllByLanguageType(LanguageTypeEntity languageType);

    List<QuestionEntity> findAllByComplexityType(ComplexityTypeEntity complexityType);

    List<QuestionEntity> findAllByQuestionType(QuestionTypeEntity questionType);

    List<QuestionEntity> findAllByStudentFormType(StudentFormTypeEntity formType);

    List<QuestionEntity> findAllByStatusType(StatusTypeEntity statusType);

    List<QuestionEntity> findAllByAuthorId(long authorId);
}
