package org.itstep.teststep.repository;

import org.itstep.teststep.entity.TestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TestRepository extends MongoRepository<TestEntity, String> {

    List<TestEntity> findAllByAuthorId(long authorId);
}
