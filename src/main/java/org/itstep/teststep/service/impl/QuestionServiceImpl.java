package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import org.itstep.teststep.converter.QuestionsConverter;
import org.itstep.teststep.entity.*;
import org.itstep.teststep.model.Question;
import org.itstep.teststep.repository.QuestionRepository;
import org.itstep.teststep.service.QuestionService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.itstep.teststep.util.ExceptionUtil.notFoundException;

@Service
@AllArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository repository;
    private final QuestionsConverter converter;

    @Override
    public Question findById(String id) {
        final QuestionEntity question = repository.findById(id).orElseThrow(() ->
                notFoundException(String.format("Question with id %d not found.", id)));
        return converter.toModel(question);
    }

    @Override
    public List<Question> findByAuthorId(long authorId) {
        final List<QuestionEntity> questions = repository.findAllByAuthorId(authorId);
        return converter.toModels(questions);
    }

    @Override
    public List<Question> findAll() {
        final List<QuestionEntity> questions = repository.findAll();
        return converter.toModels(questions);
    }

    @Override
    public List<Question> findReady() {
        final List<QuestionEntity> questions = repository.findAllByStatusType(StatusTypeEntity.READY);
        return converter.toModels(questions);
    }

    @Override
    public List<Question> findByParams(String... params) {
        final Set<ComplexityTypeEntity> complexityTypes = new HashSet<>();
        final Set<LanguageTypeEntity> languageTypes = new HashSet<>();
        final Set<QuestionTypeEntity> questionTypes = new HashSet<>();
        final Set<StudentFormTypeEntity> studentFormTypes = new HashSet<>();

        Stream.of(params).forEach(param -> {
            ComplexityTypeEntity.getComplexityTypeByName(param).ifPresent(complexityTypes::add);
            LanguageTypeEntity.getLanguageTypeByName(param).ifPresent(languageTypes::add);
            QuestionTypeEntity.getQuestionTypeByName(param).ifPresent(questionTypes::add);
            StudentFormTypeEntity.getStudentFormypeByName(param).ifPresent(studentFormTypes::add);
        });

        List<QuestionEntity> questions;
        if (languageTypes.size() == 1) questions = repository.findAllByLanguageType(languageTypes.iterator().next());
        else if (studentFormTypes.size() == 1)
            questions = repository.findAllByStudentFormType(studentFormTypes.iterator().next());
        else if (complexityTypes.size() == 1)
            questions = repository.findAllByComplexityType(complexityTypes.iterator().next());
        else if (questionTypes.size() == 1)
            questions = repository.findAllByQuestionType(questionTypes.iterator().next());
        else questions = repository.findAll();

        List<QuestionEntity> foundQuestions = questions.stream()
                .filter(q -> complexityTypes.size() == 0 || complexityTypes.contains(q.getComplexityType()))
                .filter(q -> languageTypes.size() == 0 || languageTypes.contains(q.getLanguageType()))
                .filter(q -> questionTypes.size() == 0 || questionTypes.contains(q.getQuestionType()))
                .filter(q -> studentFormTypes.size() == 0 || studentFormTypes.contains(q.getStudentFormType()))
                .collect(Collectors.toList());

        if (foundQuestions.size() == 0) throw notFoundException("No one question was found.");

        Collections.shuffle(foundQuestions);
        return converter.toModels(foundQuestions);
    }

    @Override
    public List<Question> findReadyByParams(String... params) {
        final List<QuestionEntity> questions = converter.toEntities(findByParams(params));
        final List<QuestionEntity> readyQuestions = questions.stream()
                .filter(q -> q.getStatusType().equals(StatusTypeEntity.READY))
                .collect(Collectors.toList());
        if (readyQuestions.size() == 0) throw notFoundException("No one question was found.");
        return converter.toModels(readyQuestions);
    }

    @Override
    public List<Question> findByParams(int numOfQuestions, String... params) {
        final List<QuestionEntity> questions = converter.toEntities(findByParams(params));
        final List<QuestionEntity> limitedQuestions = questions.stream().limit(numOfQuestions).collect(Collectors.toList());
        return converter.toModels(limitedQuestions);
    }

    @Override
    public List<Question> findReadyByParams(int numOfQuestions, String... params) {
        final List<QuestionEntity> questions = converter.toEntities(findReadyByParams(params));
        final List<QuestionEntity> limitedQuestions = questions.stream().limit(numOfQuestions).collect(Collectors.toList());
        return converter.toModels(limitedQuestions);
    }


    @Override
    public Question save(Question question) {
        final QuestionEntity questionToSave = converter.toEntity(question);
        final QuestionEntity savedQuestion = repository.save(questionToSave);
        return converter.toModel(savedQuestion);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
