package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import org.itstep.teststep.converter.TestsConverter;
import org.itstep.teststep.entity.TestEntity;
import org.itstep.teststep.model.Test;
import org.itstep.teststep.repository.TestRepository;
import org.itstep.teststep.service.TestService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static org.itstep.teststep.util.ExceptionUtil.notFoundException;

@Service
@AllArgsConstructor
public class TestServiceImpl implements TestService {

    private final TestRepository repository;
    private final TestsConverter converter;

    @Override
    public Test findById(String id) {
        final TestEntity test = repository.findById(id).orElseThrow(() ->
                notFoundException(String.format("Test with id %d not found.", id)));
        return converter.toModel(test);
    }

    @Override
    public List<Test> findByAuthorId(long authorId) {
        final List<TestEntity> Tests = repository.findAllByAuthorId(authorId);
        return converter.toModels(Tests);
    }

    @Override
    public List<Test> findAll() {
        final List<TestEntity> tests = repository.findAll();
        return converter.toModels(tests);
    }

    @Override
    public List<Test> findByParams(String... types) {
        //TODO
        return null;
    }

    @Override
    public Test save(Test test) {
        TestEntity testToSave = converter.toEntity(test);
        TestEntity savedTest = repository.save(testToSave);
        return converter.toModel(savedTest);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }

    @Override
    public Test generateNewTestByParams(String... types) {
        //TODO
        return null;
    }

    @Override
    public Test generateNewTestByParams(int numOfTests, String... types) {
        //TODO
        return null;
    }
}
