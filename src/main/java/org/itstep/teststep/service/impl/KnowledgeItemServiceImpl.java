package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import org.itstep.teststep.converter.KnowledgeItemConverter;
import org.itstep.teststep.entity.KnowledgeItemEntity;
import org.itstep.teststep.entity.LanguageTypeEntity;
import org.itstep.teststep.model.KnowledgeItem;
import org.itstep.teststep.model.LanguageType;
import org.itstep.teststep.repository.KnowledgeItemRepository;
import org.itstep.teststep.service.KnowledgeItemService;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.itstep.teststep.util.ExceptionUtil.notFoundException;

@Service
@AllArgsConstructor
public class KnowledgeItemServiceImpl implements KnowledgeItemService {

    private final KnowledgeItemRepository repository;
    private final KnowledgeItemConverter converter;

    @Override
    public KnowledgeItem findById(String id) {
        final KnowledgeItemEntity item = repository.findById(id).orElseThrow(() ->
                notFoundException(String.format("Item with id %d not found.", id)));
        return converter.toModel(item);
    }

    @Override
    public List<KnowledgeItem> findAll() {
        final List<KnowledgeItemEntity> items = repository.findAll();
        return converter.toModels(items);
    }

    @Override
    public List<KnowledgeItem> findByLanguageType(String languageType) {
        final LanguageTypeEntity type = LanguageTypeEntity.getLanguageTypeByName(languageType)
                .orElseThrow(() -> notFoundException(String.format("Language '%s' not found.", languageType)));
        final List<KnowledgeItemEntity> items = repository.findAllByLanguageType(type);
        return converter.toModels(items);
    }

    @Override
    public KnowledgeItem save(KnowledgeItem item) {
        final KnowledgeItemEntity itemToSave = converter.toEntity(item);
        final KnowledgeItemEntity savedItem = repository.save(itemToSave);
        return converter.toModel(savedItem);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
