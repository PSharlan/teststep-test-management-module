package org.itstep.teststep.service;

import org.itstep.teststep.model.Test;

import java.util.List;

public interface TestService {

    Test findById(String id);

    List<Test> findByAuthorId(long authorId);

    List<Test> findAll();

    List<Test> findByParams(String... types);

    Test save(Test test);

    void delete(String id);

    Test generateNewTestByParams(String... params);

    Test generateNewTestByParams(int numOfQuestions, String... params);


}
