package org.itstep.teststep.service;

import org.itstep.teststep.model.KnowledgeItem;

import java.util.List;

public interface KnowledgeItemService {

    KnowledgeItem findById(String id);

    List<KnowledgeItem> findAll();

    List<KnowledgeItem> findByLanguageType(String languageType);

    KnowledgeItem save(KnowledgeItem KnowledgeItem);

    void delete(String id);
}
