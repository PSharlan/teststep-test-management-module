package org.itstep.teststep.service;

import org.itstep.teststep.model.Question;

import java.util.List;

public interface QuestionService {

    Question findById(String id);

    List<Question> findByAuthorId(long authorId);

    List<Question> findAll();

    List<Question> findReady();

    List<Question> findByParams(String... params);

    List<Question> findReadyByParams(String... params);

    List<Question> findByParams(int numOfQuestions, String... params);

    List<Question> findReadyByParams(int numOfQuestions, String... params);

    Question save(Question question);

    void delete(String id);
}
