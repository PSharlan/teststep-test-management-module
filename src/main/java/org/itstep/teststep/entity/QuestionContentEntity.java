package org.itstep.teststep.entity;

import lombok.Data;
import org.springframework.data.annotation.TypeAlias;

import java.util.List;

@Data
@TypeAlias("QuestionContent")
public class QuestionContentEntity {
    private String task;
    private List<String> rightAnswers;
    private List<String> wrongAnswers;
    private List<String> usefulBlocks;
    private List<String> uselessBlocks;
    private List<String> linksToLearn;
}
