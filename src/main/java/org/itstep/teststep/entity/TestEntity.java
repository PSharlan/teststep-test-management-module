package org.itstep.teststep.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@TypeAlias("Test")
@Document(collection = "test")
public class TestEntity {

    @Id
    private String id;
    @Field("author_id")
    private Long authorId;

    @Field("language_type")
    private LanguageTypeEntity languageType;
    @Field("student_form_type")
    private StudentFormTypeEntity studentFormType;
    @Field("complexity_type")
    private ComplexityTypeEntity complexityType;
    @Field("test_type")
    private TestTypeEntity testType;
    @Field("status_type")
    private StatusTypeEntity statusType;
    @Field("number_of_questions")
    private int numberOfQuestions;
    @DBRef
    @Field("questions")
    private List<QuestionEntity> questions;
    @DBRef
    @Field("knowledge_item")
    private KnowledgeItemEntity knowledgeItem;
}
