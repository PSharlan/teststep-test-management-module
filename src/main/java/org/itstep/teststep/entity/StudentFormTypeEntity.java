package org.itstep.teststep.entity;

import java.util.Optional;

public enum StudentFormTypeEntity {
    JUNIOR_SCHOOL(), OLD_SCHOOL(), PRODUCTION();

    public static Optional<StudentFormTypeEntity> getStudentFormypeByName(String name) {
        switch (name.toUpperCase()) {
            case "JUNIOR_SCHOOL":
                return Optional.of(JUNIOR_SCHOOL);
            case "OLD_SCHOOL":
                return Optional.of(OLD_SCHOOL);
            case "PRODUCTION":
                return Optional.of(PRODUCTION);
            default:
                return Optional.empty();
        }
    }
}