package org.itstep.teststep.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@TypeAlias("KnowledgeItem")
@Document(collection = "knowledge_item")
public class KnowledgeItemEntity {

    @Id
    private String id;
    @Field("name")
    private String name;
    @Field("language_type")
    private LanguageTypeEntity languageType;
}
