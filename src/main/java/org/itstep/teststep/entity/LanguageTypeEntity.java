package org.itstep.teststep.entity;

import java.util.Optional;

public enum LanguageTypeEntity {
    JAVA(), HTML(), JS();

    public static Optional<LanguageTypeEntity> getLanguageTypeByName(String name) {
        switch (name.toUpperCase()) {
            case "JAVA":
                return Optional.of(JAVA);
            case "HTML":
                return Optional.of(HTML);
            case "JS":
                return Optional.of(JS);
            default:
                return Optional.empty();
        }
    }
}
