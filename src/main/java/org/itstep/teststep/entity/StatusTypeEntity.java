package org.itstep.teststep.entity;

import java.util.Optional;

public enum StatusTypeEntity {

    READY(), DEVELOPMENT();

    public static Optional<StatusTypeEntity> getStatusTypeByName(String name) {
        switch (name.toUpperCase()) {
            case "READY":
                return Optional.of(READY);
            case "DEVELOPMENT":
                return Optional.of(DEVELOPMENT);
            default:
                return Optional.empty();
        }
    }
}
