package org.itstep.teststep.entity;

public enum TestTypeEntity {
    TRAINING(), HOMEWORK(), EXAM();
}
