package org.itstep.teststep.entity;

import java.util.Optional;

public enum QuestionTypeEntity {
    CHOICE("Choose single answer."),
    MULTIPLE_CHOICE("Choose the right answers. It can be multiple."),
    GAP_FILLING("Fill the gaps."),
    MATCHING("Connect the elements."),
    TRUE_FALSE("Choose True or False."),
    OPEN_QUESTION("Answer the question."),
    ERROR_CORRECTION("Correct the errors."),
    COLLECT_THE_METHOD("Collect the method from blocks."),
    WRITE_THE_METHOD("Write the method.");

    private String description;

    QuestionTypeEntity(String description) {
        this.description = description;
    }

    public static Optional<QuestionTypeEntity> getQuestionTypeByName(String name) {
        switch (name.toUpperCase()) {
            case "CHOICE":
                return Optional.of(CHOICE);
            case "MULTIPLE_CHOICE":
                return Optional.of(MULTIPLE_CHOICE);
            case "GAP_FILLING":
                return Optional.of(GAP_FILLING);
            case "MATCHING":
                return Optional.of(MATCHING);
            case "TRUE_FALSE":
                return Optional.of(TRUE_FALSE);
            case "OPEN_QUESTION":
                return Optional.of(OPEN_QUESTION);
            case "ERROR_CORRECTION":
                return Optional.of(ERROR_CORRECTION);
            case "COLLECT_THE_METHOD":
                return Optional.of(COLLECT_THE_METHOD);
            case "WRITE_THE_METHOD":
                return Optional.of(WRITE_THE_METHOD);
            default:
                return Optional.empty();
        }
    }
}
