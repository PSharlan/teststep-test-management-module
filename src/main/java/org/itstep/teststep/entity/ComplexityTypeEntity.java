package org.itstep.teststep.entity;

import java.util.Optional;

public enum ComplexityTypeEntity {
    FUNDAMENTAL(), NOVICE(), INTERMEDIATE(), ADVANCED(), EXPERT(), EXAM();

    public static Optional<ComplexityTypeEntity> getComplexityTypeByName(String name){
        switch (name.toUpperCase()){
            case "FUNDAMENTAL": return Optional.of(FUNDAMENTAL);
            case "NOVICE": return Optional.of(NOVICE);
            case "INTERMEDIATE": return Optional.of(INTERMEDIATE);
            case "ADVANCED": return Optional.of(ADVANCED);
            case "EXPERT": return Optional.of(EXPERT);
            case "EXAM": return Optional.of(EXAM);
            default: return Optional.empty();
        }
    }
}