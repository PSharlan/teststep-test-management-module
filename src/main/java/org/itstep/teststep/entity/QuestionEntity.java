package org.itstep.teststep.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@TypeAlias("Question")
@Document(collection = "question")
public class QuestionEntity {

    @Id
    private String id;
    @Field("author_id")
    private Long authorId;

    @Field("language_type")
    private LanguageTypeEntity languageType;
    @Field("student_form_type")
    private StudentFormTypeEntity studentFormType;
    @Field("complexity_type")
    private ComplexityTypeEntity complexityType;
    @Field("question_type")
    private QuestionTypeEntity questionType;
    @Field("status_type")
    private StatusTypeEntity statusType;

    @Field("question_content")
    private QuestionContentEntity content;
    @Field("knowledge_item")
    @DBRef
    private KnowledgeItemEntity knowledgeItem;
}
