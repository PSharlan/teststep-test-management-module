package org.itstep.teststep.model;

import lombok.Data;
import javax.validation.constraints.NotNull;
import org.itstep.teststep.entity.LanguageTypeEntity;

@Data
public class KnowledgeItem {

    private String id;
    @NotNull(message = "Knowledge Item w/o Name")
    private String name;
    @NotNull(message = "Knowledge Item w/o Language type")
    private LanguageTypeEntity languageType;
}
