package org.itstep.teststep.model;

public enum TestType {
    TRAINING(), HOMEWORK(), EXAM()
}
