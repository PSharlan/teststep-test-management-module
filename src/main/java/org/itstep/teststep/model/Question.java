package org.itstep.teststep.model;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Question {

    private String id;
    @NotNull(message = "Question w/o Author ID")
    private Long authorId;

    @NotNull(message = "Question w/o Language type")
    private LanguageType languageType;
    @NotNull(message = "Question w/o Student form type")
    private StudentFormType studentFormType;
    @NotNull(message = "Question w/o Complexity type")
    private ComplexityType complexityType;
    @NotNull(message = "Question w/o Question type")
    private QuestionType questionType;
    @NotNull(message = "Question w/o Status type")
    private StatusType statusType;

    @NotNull(message = "Question w/o Content")
    private QuestionContent content;
    @NotNull(message = "Question w/o Knowledge Item")
    private KnowledgeItem knowledgeItem;
}
