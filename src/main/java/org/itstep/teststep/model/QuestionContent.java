package org.itstep.teststep.model;

import lombok.Data;
import javax.validation.constraints.NotNull;

import java.util.List;

@Data
public class QuestionContent {

    @NotNull(message = "Question content w/o Task")
    private String task;
    private List<String> rightAnswers;
    private List<String> wrongAnswers;
    private List<String> usefulBlocks;
    private List<String> uselessBlocks;
    private List<String> linksToLearn;
}
