package org.itstep.teststep.model;

public enum LanguageType {
    JAVA(), HTML(), JS()
}
