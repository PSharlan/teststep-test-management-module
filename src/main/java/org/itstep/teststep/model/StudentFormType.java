package org.itstep.teststep.model;

public enum StudentFormType {
    JUNIOR_SCHOOL(), OLD_SCHOOL(), PRODUCTION()
}