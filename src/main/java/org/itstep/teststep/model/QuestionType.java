package org.itstep.teststep.model;

public enum QuestionType {
    CHOICE("Choose single answer."),
    MULTIPLE_CHOICE("Choose the right answers. It can be multiple."),
    GAP_FILLING("Fill the gaps."),
    MATCHING("Connect the elements."),
    TRUE_FALSE("Choose True or False."),
    OPEN_QUESTION("Answer the question."),
    ERROR_CORRECTION("Correct the errors."),
    COLLECT_THE_METHOD("Collect the method from blocks."),
    WRITE_THE_METHOD("Write the method.");

    private String description;

    QuestionType(String description){
        this.description = description;
    }
}
