package org.itstep.teststep.model;

import lombok.Data;

import java.util.List;

@Data
public class Test {

    private String id;
    private Long authorId;

    private LanguageType languageType;
    private StudentFormType studentFormType;
    private ComplexityType complexityType;
    private TestType testType;
    private StatusType statusType;

    private int numberOfQuestions;
    private List<Question> questions;
    private KnowledgeItem knowledgeItem;
}
