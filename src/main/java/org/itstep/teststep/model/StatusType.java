package org.itstep.teststep.model;

public enum StatusType {
    READY(), DEVELOPMENT()
}
