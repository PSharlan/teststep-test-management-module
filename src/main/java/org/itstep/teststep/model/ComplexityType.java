package org.itstep.teststep.model;

public enum ComplexityType {
    FUNDAMENTAL(), NOVICE(), INTERMEDIATE(), ADVANCED(), EXPERT(), EXAM()
}